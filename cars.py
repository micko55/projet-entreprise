class Car():
    def __init__(self, **attributes):
        [setattr(self, k, v) for k, v in attributes.items()]

    def changeAttr(self, attr: str, value):
        """attr "company" => value = Company Class or None
        attr "employee" => value = Employee Class or None
        attr "price" => int
        attr "monthly_cost" => int
        """
        self.__setattr__(attr, value)


class CompanyCar(Car):
    def __init__(self, **attributes):
        super().__init__(**attributes)
