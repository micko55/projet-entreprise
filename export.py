import csv
import json
from company import Company


class ExportData():
    def __init__(self):
        pass

    @classmethod
    def exportAsCsv(cls):
        with open("data.csv", "w", newline="") as f:
            writer = csv.writer(f)
            for c in Company.COMPANIES:
                writer.writerow(["Company " + str(c.id + 1)])
                writer.writerow(["Mois", " Pertes", " Gains", " Employés", " Voitures de fonction", " Argent"])
                for i in range(len(c.monthHistory)):
                    writer.writerow([str(i + 1), " " + str(c.monthCostHistory[i]),
                                     " " + str(c.monthGainHistory[i]), " " + str(c.employeeHistory[i]),
                                     " " + str(c.carHistory[i]), " " + str(c.monthHistory[i])]
                                    )

    @classmethod
    def exportAsJson(cls):
        with open("data.json", "w") as f:
            dico = {"Companies": {}}
            for c in Company.COMPANIES:
                dico["Companies"]["Company " + str(c.id + 1)] = {}
                for i in range(len(c.monthHistory)):
                    dico["Companies"]["Company " + str(c.id + 1)]["Mois " + str(i + 1)] = {
                        "Pertes": c.monthCostHistory[i],
                        "Gains": c.monthGainHistory[i],
                        "Voitures de fonction": c.carHistory[i],
                        "Employés": c.employeeHistory[i],
                        "Argent": c.monthHistory[i]
                    }
            json.dump(dico, f, indent=4, ensure_ascii=False)
