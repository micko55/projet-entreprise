import random
from cars import CompanyCar
from people import Employee, Director
from desk import Desk


class Company():
    COMPANIES = []

    def __init__(self, i):
        self.id = i
        self.monthCost = 0
        self.monthGain = 0
        self.money = random.randrange(1000000, 1200000)
        self.employees = []
        self.companyCars = []
        self.desks = []
        self.monthHistory = []
        self.monthCostHistory = []
        self.monthGainHistory = []
        self.carHistory = []
        self.employeeHistory = []
        self.desksHistory = []
        self.monthlyCA = random.randrange(50000, 60000)
        self.hireEmployees()
        self.buyCompanyCars()
        self.buyDesks()
        self.director = self.setNewDirector()

    def updateMonth(self):
        print("Money =", self.money, end=", ")
        self.monthCost += self.monthlyCost()
        self.monthGain += self.monthlyGain()
        self.updateEmployees()
        self.money += self.monthGain - self.monthCost
        self.monthHistory.append(self.money)
        self.carHistory.append(len(self.companyCars))
        self.employeeHistory.append(len(self.employees))
        self.desksHistory.append(len(self.desks))
        print("Monthly Cost =", self.monthCost, end=", ")
        print("Monthly Gain =", self.monthGain, end=", ")
        print("Money =", self.money)
        self.monthCostHistory.append(self.monthCost)
        self.monthGainHistory.append(self.monthGain)
        self.monthCost = 0
        self.monthGain = 0
        n = 0
        for e in Employee.EMPLOYEES:
            if e.company is None:
                n += 1
            elif e.company is self:
                e.changeAttr("timeInCompany", e.timeInCompany + 1)
                if random.randrange(100) + e.timeInCompany >= 50:
                    e.changeAttr("efficiency", random.randrange(1, 10)/1000)
        print(n, "unemployed employees")

    def updateEmployees(self):
        """sM=Start Money, eM = End Money"""
        n = 0
        nC = 0
        nD = 0
        if self.monthCost > self.monthGain or self.money + self.monthGain - self.monthCost < 0:
            while (self.monthCost > self.monthGain or self.money + self.monthGain - self.monthCost < 0) and \
                    len(self.monthHistory) >= 1:
                if len(self.employees) <= 1:
                    break
                employee = self.employees[0]
                for e in self.employees:
                    if e.salary > employee.salary or (e.timeInCompany < employee.timeInCompany and
                                                      e.efficiency > employee.efficiency):
                        employee = e
                if employee.companyCar:
                    for car in self.companyCars:
                        if car.employee == employee:
                            self.sellCompanyCar(car)
                            nC += 1
                            break
                n += 1
                self.fireEmployee(employee, withDesk=True)
            print("fired", n, "employees, sold", n, "desks and", nC, "cars", end=", ")
        elif self.monthCost < self.monthGain:
            oldN = 0
            while self.monthCost < self.monthGain or self.money - self.monthCost + self.monthGain < 0:
                for e in Employee.EMPLOYEES:
                    if self.monthGain < self.monthCost or self.money - self.monthCost + self.monthGain < 0:
                        break
                    if e.company is None:
                        if self.monthCost + e.salary - self.monthlyCA * e.efficiency > self.monthGain or \
                                self.money - self.monthCost + self.monthGain - e.salary - self.monthlyCA * \
                                e.efficiency < 0:
                            continue
                        if len(self.desks) > len(self.employees):
                            self.hireEmployee(e)
                        else:
                            self.hireEmployee(e, withDesk=True)
                            nD += 1
                        if e.cars == 0 and len(self.companyCars) < len(self.employees):
                            e.changeAttr("companyCar", True)
                            self.buyCompanyCar(e)
                            nC += 1
                        n += 1
                if oldN == n:
                    break
                oldN = n
            print("hired", n, "employees, bought", nD, "desks and", nC, "cars", end=", ")

    def monthlyCost(self):
        for e in self.employees:
            self.monthCost += e.salary
        self.monthCost += self.director.salary
        for car in self.companyCars:
            self.monthCost += car.monthly_cost
        return self.monthCost

    def monthlyGain(self):
        total = self.monthlyCA
        for e in self.employees:
            total += self.monthlyCA * e.efficiency
        return int(total)

    @classmethod
    def createCompanies(cls):
        """Créer des entreprises"""
        for i in range(2):
            company = Company(i)
            cls.COMPANIES.append(company)

    def setNewDirector(self):
        return Director(**{"company": self.id, "salary": random.randrange(5000, 10000),
                           "companyCar": True, "cars": random.randrange(1, 5)})

    def fireEmployee(self, employee, withDesk=False):
        if employee in self.employees:
            self.employees.remove(employee)
            employee.changeAttr("timeInCompany", 0)
            for d in self.desks:
                if d.employee is not None and d.employee == employee:
                    d.changeAttr("employee", None)
                    if withDesk:
                        self.sellDesk(d)
                    break
            employee.changeAttr("company", None)
            if employee.companyCar:
                employee.changeAttr("companyCar", False)
            self.monthGain -= int(self.monthlyCA * employee.efficiency)

    def hireEmployees(self):
        for e in Employee.EMPLOYEES:
            if e.company == self.id:
                self.hireEmployee(e)

    def hireEmployee(self, employee: Employee, withDesk=False):
        price = 0
        ePercent = 0
        employee.changeAttr("company", self)
        if employee not in self.employees:
            self.employees.append(employee)
            ePercent = self.monthlyCA * employee.efficiency
            price += employee.salary
            if withDesk:
                desk = self.buyDesk(employee)
                price += desk.price
        self.monthGain += int(ePercent)
        self.monthCost += price

    def buyCompanyCars(self):
        """Buy all company cars (company)"""
        [self.buyCompanyCar(e) for e in self.employees]

    def buyCompanyCar(self, employee):
        """Buy Company Car (employee)"""
        if employee.companyCar:
            attrs = {"company": self, "employee": employee}
        else:
            if employee.cars == 0:
                employee.companyCar = True
                attrs = {"company": self, "employee": employee}
            else:
                attrs = {"company": self, "employee": None}
        if attrs["employee"] is None:
            return None
        attrs["price"] = random.randrange(15000, 20000)
        attrs["monthly_cost"] = random.randrange(200, 500)
        car = CompanyCar(**attrs)
        self.companyCars.append(car)
        self.monthCost += car.price

    def sellCompanyCar(self, car):
        self.monthGain += car.price
        car.changeAttr("employee", None)
        self.companyCars.remove(car)

    def buyDesks(self):
        [self.buyDesk(e) for e in self.employees]

    def buyDesk(self, employee):
        attrs = {
            "employee": employee,
            "price": random.randrange(500, 1000)
        }
        desk = Desk(**attrs)
        self.desks.append(desk)
        self.monthCost += desk.price
        return desk

    def sellDesk(self, desk):
        self.monthGain += desk.price
        desk.changeAttr("employee", None)
        self.desks.remove(desk)
