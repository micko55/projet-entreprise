import matplotlib as mil
mil.use('TkAgg')
import matplotlib.pyplot as plt
from company import Company
import constants as C


class Graph():
    def __init__(self, **args):
        [setattr(self, k, v) for k, v in args.items()]

    def showGraph(self):
        xValues = []
        for i in range(C.MAX_MONTHS):
            xValues.append(i)
        for c in Company.COMPANIES:
            f = plt.figure(c.id + 1, figsize=[0.58 * C.MAX_MONTHS, 7])
            w = f.get_figwidth() * f.dpi
            fid = plt.gcf().number
            x = (fid - 1) * w + 10 * fid
            f.canvas.manager.window.wm_geometry("+%d+%d" % (x, 0))
            yValues = [None] * C.MAX_MONTHS
            for i in range(len(c.monthHistory)):
                yValues[i] = c.monthHistory[i]
            self.createSubplot([15, 1, (2, 5)], xValues, yValues, "Company " + str(c.id + 1), self.xName, self.yName)
            for i in range(len(c.carHistory)):
                yValues[i] = c.carHistory[i]
            self.createSubplot([15, 1, (8, 10)], xValues, yValues, label="Cars", xname=self.xName, yname="Cars",
                               color="y")
            for i in range(len(c.employeeHistory)):
                yValues[i] = c.employeeHistory[i]
            self.createPlot(xValues, yValues, label="Employees", xname=self.xName, color="m")
            """for i in range(len(c.desksHistory)):
                yValues[i] = c.desksHistory[i]
            self.createPlot(xValues, yValues, label="Desks", xname=self.xName, color="y")"""
            for i in range(len(c.monthCostHistory)):
                yValues[i] = c.monthCostHistory[i]
            self.createSubplot([15, 1, (13, 15)], xValues, yValues, "Cost", self.xName, self.yName, color="r",
                               yLimit=500000)
            for i in range(len(c.monthGainHistory)):
                yValues[i] = c.monthGainHistory[i]
            self.createPlot(xValues, yValues, label="Gain", xname=self.xName, color="g", yLimit=500000)
            plt.grid(True)
        plt.figure(1)
        plt.show()

    def createSubplot(self, i, xvalues, yvalues, label="", xname="", yname="", title="", color=None, yLimit=0):
        if i is int:
            plt.subplot(i)
        else:
            plt.subplot(i[0], i[1], i[2])
        self.createPlot(xvalues, yvalues, label, xname, yname, title, color=color, yLimit=yLimit)

    @staticmethod
    def createPlot(xvalues, yvalues, label="", xname="", yname="", title="", color=None, yLimit=0):
        plt.plot(xvalues, yvalues, marker="o", label=label, color=color)
        plt.grid(True)
        plt.figlegend(loc="upper center")
        plt.xlabel(xname)
        plt.ylabel(yname)
        plt.title(title)
        plt.grid(True)
        plt.tick_params(axis='y')
        maxRange = []
        for i in range(C.MAX_MONTHS):
            maxRange.append(i + 1)
        plt.xticks(xvalues, maxRange)
        if yLimit > 0:
            plt.ylim(0, yLimit)
            for x, y in zip(xvalues, yvalues):
                baseY = y
                textY = 10
                if y > yLimit:
                    textY = 60
                    baseY = 0
                    plt.arrow(x=x, y=y, dx=0, dy=-y + yLimit / 5 * 4, width=0.05, head_length=50000, zorder=2,
                              color=color)
                if color == "r" and y <= yLimit:
                    plt.annotate(y, (x, baseY), textcoords="offset pixels", xytext=(0, -textY), ha='center',
                                 va="center_baseline", size=8, color="r")
                else:
                    plt.annotate(y, (x, baseY), textcoords="offset pixels", xytext=(0, textY), ha='center', size=8,
                                 color=color)
            return

        for x, y in zip(xvalues, yvalues):
            plt.annotate(y, (x, y), textcoords="offset pixels", xytext=(0, 10), ha='center', size=8)
