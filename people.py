import random


class Employee():
    EMPLOYEES = []

    def __init__(self, **attributes):
        [setattr(self, k, v) for k, v in attributes.items()]

    @classmethod
    def initializeEmployees(cls):
        for i in range(random.randrange(100, 150)):
            cls.createEmployee(company=random.choice(seq=[0, 1, None]))

    @classmethod
    def createEmployee(cls, company=None):
        cars = random.randrange(0, 3)
        if cars == 0:
            attrs = {"company": company, "salary": random.randrange(1200, 1600),
                     "companyCar": True, "cars": cars, "efficiency": random.randrange(1, 50) / 1000,
                     "timeInCompany": 0}
        else:
            attrs = {"company": company, "salary": random.randrange(1200, 1600),
                     "companyCar": random.choice(seq=[True, False]), "cars": cars,
                     "efficiency": random.randrange(1, 50) / 1000, "timeInCompany": 0}
        employee = Employee(**attrs)
        cls.EMPLOYEES.append(employee)

    def changeAttr(self, attr: str, value):
        """attr "company" => value = Company class or None
        attr "salary" => value = int
        attr "companyCar" => value = True/False
        attr "cars" => value = int
        attr "efficiency" => value = float
        attr "timeInCompany" => value = int"""
        self.__setattr__(attr, value)


class Director(Employee):
    def __init__(self, **attributes):
        super().__init__(**attributes)
