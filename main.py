# import time
import random
from people import Employee
from company import Company
from graph import Graph
import constants as C
from export import ExportData


def main():
    Employee.initializeEmployees()
    Company.createCompanies()
    month = 0
    while month < C.MAX_MONTHS:
        print("Mois", month+1, ":")
        for c in Company.COMPANIES:
            newEmployees = random.randrange(10)
            for i in range(newEmployees):
                Employee.createEmployee(None)
            c.updateMonth()
        month += 1
    args = {"title": "Companies Revenue", "xName": "Mois", "yName": "Revenus"}
    ExportData.exportAsCsv()
    ExportData.exportAsJson()
    graph = Graph(**args)
    graph.showGraph()


if __name__ == "__main__":
    main()
