class Desk():
    def __init__(self, **attributes):
        [setattr(self, k, v) for k, v in attributes.items()]

    def changeAttr(self, attr: str, value):
        """ attr "employee" => Employee Class or None,
            attr "price" => int"""
        self.__setattr__(attr, value)
